package com.jeesite.common.constant;

public class Constant {
    private static final Constant INSTANCE = new Constant();

    // 供应商类型
    public class SupplyType {
        public static final String Terminal = "0"; // 终端
        public static final String SIMCard = "1";  // SIM卡
    }
}

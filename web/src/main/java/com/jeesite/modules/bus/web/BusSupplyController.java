/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.bus.entity.BusSupply;
import com.jeesite.modules.bus.service.BusSupplyService;

/**
 * 供应商信息表Controller
 * @author duwenkai
 * @version 2019-07-26
 */
@Controller
@RequestMapping(value = "${adminPath}/bus/busSupply")
public class BusSupplyController extends BaseController {

	@Autowired
	private BusSupplyService busSupplyService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public BusSupply get(String supplyCode, boolean isNewRecord) {
		return busSupplyService.get(supplyCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("bus:busSupply:view")
	@RequestMapping(value = {"list", ""})
	public String list(BusSupply busSupply, Model model) {
		model.addAttribute("busSupply", busSupply);
		return "modules/bus/busSupplyList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("bus:busSupply:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<BusSupply> listData(BusSupply busSupply, HttpServletRequest request, HttpServletResponse response) {
		busSupply.setPage(new Page<>(request, response));
		Page<BusSupply> page = busSupplyService.findPage(busSupply);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("bus:busSupply:view")
	@RequestMapping(value = "form")
	public String form(BusSupply busSupply, Model model) {
		model.addAttribute("busSupply", busSupply);
		return "modules/bus/busSupplyForm";
	}

	/**
	 * 保存供应商信息表
	 */
	@RequiresPermissions("bus:busSupply:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated BusSupply busSupply) {
		busSupplyService.save(busSupply);
		return renderResult(Global.TRUE, text("保存供应商信息表成功！"));
	}
	
	/**
	 * 删除供应商信息表
	 */
	@RequiresPermissions("bus:busSupply:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(BusSupply busSupply) {
		busSupplyService.delete(busSupply);
		return renderResult(Global.TRUE, text("删除供应商信息表成功！"));
	}
	
}
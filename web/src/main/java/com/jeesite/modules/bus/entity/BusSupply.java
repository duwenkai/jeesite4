/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.entity;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import com.jeesite.common.entity.BaseEntity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 供应商信息表Entity
 * @author duwenkai
 * @version 2019-07-26
 */
@Table(name="bus_supply", alias="a", columns={
		@Column(name="supply_code", attrName="supplyCode", label="供应商编码", isPK=true),
		@Column(name="supply_name", attrName="supplyName", label="供应商名称", queryType=QueryType.LIKE),
		@Column(name="supply_short_name", attrName="supplyShortName", label="供应商简称", queryType=QueryType.LIKE),
		@Column(name="address", attrName="address", label="供应商地址", isQuery=false),
		@Column(name="contact_name", attrName="contactName", label="联系人姓名", queryType=QueryType.LIKE),
		@Column(name="contact_tel", attrName="contactTel", label="联系人电话"),
		@Column(name="supply_type", attrName="supplyType", label="供应商类型", comment="供应商类型（0终端、1SIM卡）"),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
		@Column(name="remarks", attrName="remarks", label="备注信息", queryType=QueryType.LIKE),
		@Column(includeEntity=BaseEntity.class),
	}, orderBy="a.update_date DESC"
)
public class BusSupply extends DataEntity<BusSupply> {
	
	private static final long serialVersionUID = 1L;
	private String supplyCode;		// 供应商编码
	private String supplyName;		// 供应商名称
	private String supplyShortName;		// 供应商简称
	private String address;		// 供应商地址
	private String contactName;		// 联系人姓名
	private String contactTel;		// 联系人电话
	private String supplyType;		// 供应商类型（0终端、1SIM卡）
	
	public BusSupply() {
		this(null);
	}

	public BusSupply(String id){
		super(id);
	}
	
	public String getSupplyCode() {
		return supplyCode;
	}

	public void setSupplyCode(String supplyCode) {
		this.supplyCode = supplyCode;
	}
	
	@NotBlank(message="供应商名称不能为空")
	@Length(min=0, max=100, message="供应商名称长度不能超过 100 个字符")
	public String getSupplyName() {
		return supplyName;
	}

	public void setSupplyName(String supplyName) {
		this.supplyName = supplyName;
	}
	
	@NotBlank(message="供应商简称不能为空")
	@Length(min=0, max=100, message="供应商简称长度不能超过 100 个字符")
	public String getSupplyShortName() {
		return supplyShortName;
	}

	public void setSupplyShortName(String supplyShortName) {
		this.supplyShortName = supplyShortName;
	}
	
	@NotBlank(message="供应商地址不能为空")
	@Length(min=0, max=100, message="供应商地址长度不能超过 100 个字符")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@NotBlank(message="联系人姓名不能为空")
	@Length(min=0, max=50, message="联系人姓名长度不能超过 50 个字符")
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	@NotBlank(message="联系人电话不能为空")
	@Length(min=0, max=20, message="联系人电话长度不能超过 20 个字符")
	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}
	
	@NotBlank(message="供应商类型不能为空")
	@Length(min=0, max=1, message="供应商类型长度不能超过 1 个字符")
	public String getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(String supplyType) {
		this.supplyType = supplyType;
	}
	
}
/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.bus.entity.BusTerminalType;

/**
 * 终端类型信息表DAO接口
 * @author duwenkai
 * @version 2019-07-27
 */
@MyBatisDao
public interface BusTerminalTypeDao extends CrudDao<BusTerminalType> {
	
}
/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.bus.entity.BusCustomer;
import com.jeesite.modules.bus.service.BusCustomerService;

/**
 * 客户信息表Controller
 * @author duwenkai
 * @version 2019-07-26
 */
@Controller
@RequestMapping(value = "${adminPath}/bus/busCustomer")
public class BusCustomerController extends BaseController {

	@Autowired
	private BusCustomerService busCustomerService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public BusCustomer get(String customerCode, boolean isNewRecord) {
		return busCustomerService.get(customerCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("bus:busCustomer:view")
	@RequestMapping(value = {"list", ""})
	public String list(BusCustomer busCustomer, Model model) {
		model.addAttribute("busCustomer", busCustomer);
		return "modules/bus/busCustomerList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("bus:busCustomer:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<BusCustomer> listData(BusCustomer busCustomer, HttpServletRequest request, HttpServletResponse response) {
		busCustomer.setPage(new Page<>(request, response));
		Page<BusCustomer> page = busCustomerService.findPage(busCustomer);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("bus:busCustomer:view")
	@RequestMapping(value = "form")
	public String form(BusCustomer busCustomer, Model model) {
		model.addAttribute("busCustomer", busCustomer);
		return "modules/bus/busCustomerForm";
	}

	/**
	 * 保存客户信息表
	 */
	@RequiresPermissions("bus:busCustomer:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated BusCustomer busCustomer) {
		busCustomerService.save(busCustomer);
		return renderResult(Global.TRUE, text("保存客户信息表成功！"));
	}
	
	/**
	 * 删除客户信息表
	 */
	@RequiresPermissions("bus:busCustomer:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(BusCustomer busCustomer) {
		busCustomerService.delete(busCustomer);
		return renderResult(Global.TRUE, text("删除客户信息表成功！"));
	}
	
}
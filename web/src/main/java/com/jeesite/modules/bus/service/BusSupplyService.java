/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.bus.entity.BusSupply;
import com.jeesite.modules.bus.dao.BusSupplyDao;

/**
 * 供应商信息表Service
 * @author duwenkai
 * @version 2019-07-26
 */
@Service
@Transactional(readOnly=true)
public class BusSupplyService extends CrudService<BusSupplyDao, BusSupply> {
	
	/**
	 * 获取单条数据
	 * @param busSupply
	 * @return
	 */
	@Override
	public BusSupply get(BusSupply busSupply) {
		return super.get(busSupply);
	}
	
	/**
	 * 查询分页数据
	 * @param busSupply 查询条件
	 * @param busSupply.page 分页对象
	 * @return
	 */
	@Override
	public Page<BusSupply> findPage(BusSupply busSupply) {
		return super.findPage(busSupply);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param busSupply
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(BusSupply busSupply) {
		super.save(busSupply);
	}
	
	/**
	 * 更新状态
	 * @param busSupply
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(BusSupply busSupply) {
		super.updateStatus(busSupply);
	}
	
	/**
	 * 删除数据
	 * @param busSupply
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(BusSupply busSupply) {
		super.delete(busSupply);
	}
	
}
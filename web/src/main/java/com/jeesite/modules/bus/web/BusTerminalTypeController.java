/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.constant.Constant;
import com.jeesite.modules.bus.entity.BusSupply;
import com.jeesite.modules.bus.service.BusSupplyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.bus.entity.BusTerminalType;
import com.jeesite.modules.bus.service.BusTerminalTypeService;

/**
 * 终端类型信息表Controller
 * @author duwenkai
 * @version 2019-07-27
 */
@Controller
@RequestMapping(value = "${adminPath}/bus/busTerminalType")
public class BusTerminalTypeController extends BaseController {

	@Autowired
	private BusTerminalTypeService busTerminalTypeService;
	@Autowired
	private BusSupplyService busSupplyService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public BusTerminalType get(String terminalTypeId, boolean isNewRecord) {
		return busTerminalTypeService.get(terminalTypeId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("bus:busTerminalType:view")
	@RequestMapping(value = {"list", ""})
	public String list(BusTerminalType busTerminalType, Model model) {
        // 获取终端供应商列表
        BusSupply busSupply = new BusSupply();
        busSupply.setSupplyType(Constant.SupplyType.Terminal);
        model.addAttribute("busSupplyList", busSupplyService.findList(busSupply));
		model.addAttribute("busTerminalType", busTerminalType);
		return "modules/bus/busTerminalTypeList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("bus:busTerminalType:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<BusTerminalType> listData(BusTerminalType busTerminalType, HttpServletRequest request, HttpServletResponse response) {
		busTerminalType.setPage(new Page<>(request, response));
		Page<BusTerminalType> page = busTerminalTypeService.findPage(busTerminalType);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("bus:busTerminalType:view")
	@RequestMapping(value = "form")
	public String form(BusTerminalType busTerminalType, Model model) {
		// 获取终端供应商列表
		BusSupply busSupply = new BusSupply();
		busSupply.setSupplyType(Constant.SupplyType.Terminal);
		model.addAttribute("busSupplyList", busSupplyService.findList(busSupply));
		model.addAttribute("busTerminalType", busTerminalType);
		return "modules/bus/busTerminalTypeForm";
	}

	/**
	 * 保存终端类型
	 */
	@RequiresPermissions("bus:busTerminalType:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated BusTerminalType busTerminalType) {
		busTerminalTypeService.save(busTerminalType);
		return renderResult(Global.TRUE, text("保存终端类型成功！"));
	}
	
	/**
	 * 删除终端类型
	 */
	@RequiresPermissions("bus:busTerminalType:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(BusTerminalType busTerminalType) {
		busTerminalTypeService.delete(busTerminalType);
		return renderResult(Global.TRUE, text("删除终端类型成功！"));
	}
	
}
/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.entity;

import javax.validation.constraints.NotBlank;

import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.modules.sys.entity.Area;
import com.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;
import com.jeesite.common.entity.BaseEntity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 终端类型信息表Entity
 * @author duwenkai
 * @version 2019-07-27
 */
@Table(name="bus_terminal_type", alias="a", columns={
		@Column(name="terminal_type_id", attrName="terminalTypeId", label="终端类型id", isPK=true),
		@Column(name="supply_code", attrName="supply.supplyCode", label="供应商id"),
		@Column(name="terminal_type_code", attrName="terminalTypeCode", label="终端型号", queryType=QueryType.LIKE),
		@Column(name="material_number", attrName="materialNumber", label="物料号", queryType=QueryType.LIKE),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
		@Column(name="remarks", attrName="remarks", label="备注信息", queryType=QueryType.LIKE),
		@Column(includeEntity=BaseEntity.class),
	}, joinTable={
		@JoinTable(type= JoinTable.Type.LEFT_JOIN, entity=BusSupply.class, alias="b",
				on="b.supply_code = a.supply_code", attrName = "supply",
				columns={
						@Column(name="supply_code", label="供应商id", isPK=true),
						@Column(name="supply_short_name", label="供应商名称"),
				})
	}, orderBy="a.update_date DESC"
)
public class BusTerminalType extends DataEntity<BusTerminalType> {
	
	private static final long serialVersionUID = 1L;
	private String terminalTypeId;		// 终端类型id
	private BusSupply supply;		// 供应商
	private String terminalTypeCode;		// 终端型号
	private String materialNumber;		// 物料号
	
	public BusTerminalType() {
		this(null);
	}

	public BusTerminalType(String id){
		super(id);
	}
	
	public String getTerminalTypeId() {
		return terminalTypeId;
	}

	public void setTerminalTypeId(String terminalTypeId) {
		this.terminalTypeId = terminalTypeId;
	}

	public BusSupply getSupply() {
		return supply;
	}

	public void setSupply(BusSupply supply) {
		this.supply = supply;
	}

	@NotBlank(message="终端型号不能为空")
	@Length(min=0, max=100, message="终端型号长度不能超过 100 个字符")
	public String getTerminalTypeCode() {
		return terminalTypeCode;
	}

	public void setTerminalTypeCode(String terminalTypeCode) {
		this.terminalTypeCode = terminalTypeCode;
	}
	
	@NotBlank(message="物料号不能为空")
	@Length(min=0, max=100, message="物料号长度不能超过 100 个字符")
	public String getMaterialNumber() {
		return materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	
}
/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.bus.entity.BusTerminalType;
import com.jeesite.modules.bus.dao.BusTerminalTypeDao;

/**
 * 终端类型信息表Service
 * @author duwenkai
 * @version 2019-07-27
 */
@Service
@Transactional(readOnly=true)
public class BusTerminalTypeService extends CrudService<BusTerminalTypeDao, BusTerminalType> {
	
	/**
	 * 获取单条数据
	 * @param busTerminalType
	 * @return
	 */
	@Override
	public BusTerminalType get(BusTerminalType busTerminalType) {
		return super.get(busTerminalType);
	}
	
	/**
	 * 查询分页数据
	 * @param busTerminalType 查询条件
	 * @param busTerminalType.page 分页对象
	 * @return
	 */
	@Override
	public Page<BusTerminalType> findPage(BusTerminalType busTerminalType) {
		return super.findPage(busTerminalType);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param busTerminalType
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(BusTerminalType busTerminalType) {
		super.save(busTerminalType);
	}
	
	/**
	 * 更新状态
	 * @param busTerminalType
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(BusTerminalType busTerminalType) {
		super.updateStatus(busTerminalType);
	}
	
	/**
	 * 删除数据
	 * @param busTerminalType
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(BusTerminalType busTerminalType) {
		super.delete(busTerminalType);
	}
	
}
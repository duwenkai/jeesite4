/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.bus.entity.BusCustomer;
import com.jeesite.modules.bus.dao.BusCustomerDao;

/**
 * 客户信息表Service
 * @author duwenkai
 * @version 2019-07-26
 */
@Service
@Transactional(readOnly=true)
public class BusCustomerService extends CrudService<BusCustomerDao, BusCustomer> {
	
	/**
	 * 获取单条数据
	 * @param busCustomer
	 * @return
	 */
	@Override
	public BusCustomer get(BusCustomer busCustomer) {
		return super.get(busCustomer);
	}
	
	/**
	 * 查询分页数据
	 * @param busCustomer 查询条件
	 * @param busCustomer.page 分页对象
	 * @return
	 */
	@Override
	public Page<BusCustomer> findPage(BusCustomer busCustomer) {
		return super.findPage(busCustomer);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param busCustomer
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(BusCustomer busCustomer) {
		super.save(busCustomer);
	}
	
	/**
	 * 更新状态
	 * @param busCustomer
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(BusCustomer busCustomer) {
		super.updateStatus(busCustomer);
	}
	
	/**
	 * 删除数据
	 * @param busCustomer
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(BusCustomer busCustomer) {
		super.delete(busCustomer);
	}
	
}
/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.bus.entity;

import javax.validation.constraints.NotBlank;

import com.jeesite.modules.sys.entity.Area;
import org.hibernate.validator.constraints.Length;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.jeesite.common.entity.BaseEntity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 客户信息表Entity
 * @author duwenkai
 * @version 2019-07-26
 */
@Table(name="bus_customer", alias="a", columns={
		@Column(name="customer_code", attrName="customerCode", label="客户编码", isPK=true),
		@Column(name="customer_name", attrName="customerName", label="客户名称", queryType=QueryType.LIKE),
		@Column(name="customer_short_name", attrName="customerShortName", label="客户简称", queryType=QueryType.LIKE),
		@Column(name="address", attrName="address", label="客户地址", isQuery=false),
		@Column(name="contact_name", attrName="contactName", label="联系人姓名", queryType=QueryType.LIKE),
		@Column(name="contact_tel", attrName="contactTel", label="联系人电话"),
		@Column(name="area_code", 		attrName="area.areaCode", 	label="区域编码"),
		@Column(name="sale_user_code", attrName="saleUserCode.userCode", label="销售经理编码"),
		@Column(name="customer_status", attrName="customerStatus", label="客户状态", comment="客户状态（0售前、1已签）"),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
		@Column(name="remarks", attrName="remarks", label="备注信息", queryType=QueryType.LIKE),
		@Column(includeEntity=BaseEntity.class),
	}, joinTable={
		@JoinTable(type=Type.LEFT_JOIN, entity=Area.class, alias="b",
				on="b.area_code = a.area_code",
				columns={
						@Column(name="area_code", label="区域代码", isPK=true),
						@Column(name="area_name", label="区域名称", isQuery=false),
						@Column(name="area_type", label="区域类型"),
						@Column(name="tree_names", label="区域全称"),
				}),
		@JoinTable(type=Type.LEFT_JOIN, entity=User.class, attrName="saleUserCode", alias="u8",
			on="u8.user_code = a.sale_user_code", columns={
				@Column(name="user_code", label="用户编码", isPK=true),
				@Column(name="user_name", label="用户名称", isQuery=false),
		}),
	}, orderBy="a.update_date DESC"
)
public class BusCustomer extends DataEntity<BusCustomer> {
	
	private static final long serialVersionUID = 1L;
	private String customerCode;		// 客户编码
	private String customerName;		// 客户名称
	private String customerShortName;		// 客户简称
	private String address;		// 客户地址
	private String contactName;		// 联系人姓名
	private String contactTel;		// 联系人电话
	private Area area;				// 区域编码
	private User saleUserCode;		// 销售经理编码
	private String customerStatus;		// 客户状态（0售前、1已签）
	
	public BusCustomer() {
		this(null);
	}

	public BusCustomer(String id){
		super(id);
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	@NotBlank(message="客户名称不能为空")
	@Length(min=0, max=100, message="客户名称长度不能超过 100 个字符")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@NotBlank(message="客户简称不能为空")
	@Length(min=0, max=100, message="客户简称长度不能超过 100 个字符")
	public String getCustomerShortName() {
		return customerShortName;
	}

	public void setCustomerShortName(String customerShortName) {
		this.customerShortName = customerShortName;
	}
	
	@Length(min=0, max=100, message="客户地址长度不能超过 100 个字符")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@NotBlank(message="联系人姓名不能为空")
	@Length(min=0, max=50, message="联系人姓名长度不能超过 50 个字符")
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	@NotBlank(message="联系人电话不能为空")
	@Length(min=0, max=20, message="联系人电话长度不能超过 20 个字符")
	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	public User getSaleUserCode() {
		return saleUserCode;
	}

	public void setSaleUserCode(User saleUserCode) {
		this.saleUserCode = saleUserCode;
	}
	
	@NotBlank(message="客户状态不能为空")
	@Length(min=0, max=1, message="客户状态长度不能超过 1 个字符")
	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	
}